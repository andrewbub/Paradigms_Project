// written by: Andrew Bub, Harry Gebremedhin, Molly Pierce  

//links used for requests 
var service = "student04.cse.nd.edu:51035/"
var s = service + "/states/"
var c = service + "/cost/"
var sa = service + "/sat/"
var ac = service + "/act/"
var reg = service + "/regions/"
var siz = service + "/colleges/" // how did we filterls by this 

// onlcick controller for the findschool button 
// remember to make the school text form wider 
function findschool(region, state, ACT, SAT, tuition, size){
    var xhregion = new XMLHttpRequest()
    if (region != null){
        xhregion.open("GET", service+region,true)
        xhregion.onload = function(e){
            var responseregion = JSON.parse(xhregion.responseText)

        }
        xhregion.onerror = function(e){
            console.log("loading of region request failed")
        }
        xhrregion.send(null)
    }

    var xhrstate = new XMLHttpRequest()
    if (state != null){
        xhrstate.open("GET", service+state, true)
        xhrstate.onload = function(e){
            var responsestate = JSON.parse(xhrstate.responseText)
        }
        xhrstate.onerror = function(e){
            console.log("loading of state request failed")
        }
        xhrstate.send(null)
    }

    var xhrACT = new XMLHttpRequest()
    if (ACT != null){
        xhrACT.open("GET", service+ACT, true)
        xhrACT.onload = function(e){
            var responseACT = JSON.parse(xhrACT.responseText)
        }
        xhrACT.onerror = function(e){
            console.log("loading of ACT request failed")
        } 
        xhrACT.send(null)
    }
    var xhrSAT = new XMLHttpRequest()
    if (SAT != null){
        xhrSAT.open("GET", service+SAT, true)
        xhrSAT.onload = function(e){
            var responseSAT = JSON.parse(xhrSAT.responseText)
        }
        xhrSAT.onerror = function(e){
            console.log("loading of SAT request failed")
        } 
        xhrSAT.send(null)
    }
    var xhrtuition = new XMLHttpRequest()
    if (tuition != null){
        xhrtuition.open("GET", service+SAT, true)
        xhrtuition.onload = function(e){
            var responsetuition = JSON.parse(xhrtuition.responseText)
        }
        xhrtuition.onerror = function(e){
            console.log("loading of tuition request failed")
        } 
        xhrtuition.send(null)
    }
    var xhrsize = new XMLHttpRequest()
    if (size != null){
        xhrsize.open("GET", service+size, true)
        xhrsize.onload = function(e){
            var responsesize = JSON.parse(xhrsize.responseText)
        }
        xhrsize.onerror = function(e){
            console.log("loading of size request failed")
        } 
        xhrsize.send(null)

    }

    return responseregion,responsestate, responseACT, responseSAT, responsetuition, responsesize

}

function Getinfo(schoolname){
    xhrschool = new XMLHttpRequest()
    xhrschool.open("GET", siz+schoolname, true)
    xhrschool.onload = function(){
        var responseinfo = JSON.parse(xhr.xhrschool)
    }
    xhrschool.onerror = function(){
        console.log("Get info function failed")
    }
    xhrschool.send(null)

    return responseinfo

}


// dynamically creating web site elements 
Item();
Button();
textbox();
Radio();
Dropdown();
Label();
DIV();

//inheriting the addtoDocument property
Button.prototype=Item();
textbox.prototype=Item();
Radio.prototype=Item();
Dropdown.prototype=Item();
Label.prototype=Item();
DIV.prototype=Item();

//creating the different divs
var topFDiv= new DIV()
this.createDiv('topFDiv');
this.addtoDocument();

var leftFDiv= new DIV()
this.createDiv('leftFDiv');
this.addtoDocument();

var midFDiv= new DIV()
this.createDiv('midFDiv');
this.addtoDocument();

var rightFDiv= new DIV()
this.createDiv('rightFDiv');
this.addtoDocument();

var bottomFDiv= new DIV()
this.createDiv('bottomFDiv');
this.addtoDocument();

var orDiv= new DIV()
this.createDiv('orDiv');
this.addtoDocument();

var topGDiv= new DIV()
this.createDiv('topGDiv');
this.addtoDocument();

var bottomGDiv= new DIV()
this.createDiv('bottomGDiv');
this.addtoDocument();


//dynamically creating web site elements

// Top Find Division
var Find = new Label();
this.createLabel('Find Schools','Find');
this.addtodiv('topFDiv');


// Left Find Division 
var Region = new Label();
this.createLabel('Region:','Region');
this.addtodiv('leftFDiv');

var region = new Dropdown();
this.createDropdown({'Select Region': null, 'New England': 1, 'Mid-Atlantic': 2, 'Great Lakes': 3, 'Plains' : 4, 'Southeast' : 5, 'Southwest': 6, 'Rocky Mountains': 7, 'Far West': 8, 'Outlying Areas': 9}, 'Region: ', 'region', 1);
this.addtodiv('leftFDiv');

var blank = new Label();
this.createLabel(' ','blank');
this.addtodiv('leftFDiv');

var ACT = new textbox();
this.createtextbox("ACT", ['Enter ACT: '], ['ACT'], 'ACT');
this.addtodiv('leftFDiv');

var blank = new Label();
this.createLabel(' ','blank');
this.addtodiv('leftFDiv');

var cost = new Radio();
this.createRadio("Affordable Tuition:", ['Less than $25,000', '$25,000 - $50,000', 'More than $50,000'], size);
this.addtodiv('leftFDiv');


// Middle Find Division
var City = new Label();
this.createLabel('City:','City');
this.addtodiv('midFDiv');

var city = new textbox();
this.createtextbox("city", [''], ['city'], 'city');
this.addtodiv('midFDiv');


// Right Find Division
var State = new Label();
this.createLabel('State:','State');
this.addtodiv('rightFDiv');

var state = new Dropdown();
this.createDropdown({'Select State': null, 'AK': 'AK', 'AL': 'AL', 'AR': 'AR', 'AZ': 'AZ', 'CA': 'CA', 'CO': 'CO', 'CT': 'CT', 'DE': 'DE', 'FL': 'FL', 'GA': 'GA', 'HI': 'HI', 'IA': 'IA', 'ID': 'ID', 'IL': 'IL', 'IN': 'IN', 'KS': 'KS', 'KY': 'KY', 'LA': 'LA', 'MA': 'MA', 'MD': 'MD', 'ME': 'ME', 'MI': 'MI', 'MN': 'MN', 'MO': 'MO', 'MS': 'MS', 'MT': 'MT', 'NC': 'NC', 'ND': 'ND', 'NE': 'NE', 'NH': 'NH', 'NJ': 'NJ', 'NM': 'NM', 'NV' : 'NV', 'NY': 'NY', 'OH': 'OH', 'OK': 'OK', 'OR': 'OR', 'PA': 'PA', 'RI': 'RI', 'SC': 'SC', 'SD': 'SD', 'TN': 'TN', 'TX': 'TX', 'UT': 'UT', 'VA': 'VA', 'VT': 'VT', 'WA': 'WA', 'WI': 'WI', 'WV': 'WV', 'WY': 'WY'}, 'State: ', 'state', 1);

this.addtodiv('rightFDiv');

var blank = new Label();
this.createLabel(' ','blank');
this.addtodiv('rightFDiv');

var SAT = new textbox();
this.createtextbox("SAT", ['Enter SAT: '], ['SAT'], 'SAT');
this.addtodiv('rightFDiv');

var blank = new Label();
this.createLabel(' ','blank');
this.addtodiv('rightFDiv');

var size = new Radio();
this.createRadio('Size of School: ', ['Small (Less than 2,000)', 'Medium (2,000 - 15,000)', 'Large (More than 15,000)'], size);
this.addtodiv('rightFDiv');


// Bottom Find Division
var submitF = new Button();
this.createButton('Find Schools','SUBMIT');
this.addtodiv('bottomFDiv');


// Or Division
var Or = new Label();
this.createLabel('OR','Or');
this.addtodiv('orDiv');


// Top Get Division
var Get = new Label();
this.createLabel('Get School Information','Get');
this.addtodiv('topGDiv');


// Bottom Get Division
var School = new Label();
this.createLabel('School Name:','School');
this.addtodiv('bottomGDiv');

var school = new textbox();
this.createtextbox("school", [' '], ['school'], 'school');
this.addtodiv('bottomGDiv');

var blank = new Label();
this.createLabel(' ','blank');
this.addtodiv('bottomGDiv');

var submitG = new Button();
this.createButton('Get Info','SUBMIT');
this.addtodiv('bottomGDiv');
//this.addClickEventHandler(Getinfo(school.getValue()))


